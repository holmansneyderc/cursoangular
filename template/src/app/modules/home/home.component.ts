import { Component, OnInit } from '@angular/core';
import { Home } from 'src/app/shared/models/home';
import { PageService } from 'src/app/shared/services/page.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public data: Home = new Home();
  public status: number = 0;
  constructor(private _pageService: PageService) { }

  ngOnInit(): void {
    this._pageService.getPage().subscribe((data:any)=>{
      this.data = data.content;
      this.status = data.status
    })
  }

  
}
