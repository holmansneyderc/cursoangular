import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PageService } from 'src/app/shared/services/page.service';
import { Home } from 'src/app/shared/models/home';
import { PodcastComponent } from './podcast/podcast.component';
import { FeedsComponent } from './feeds/feeds.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgxMasonryModule } from 'ngx-masonry';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


fdescribe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let httpTestingController: HttpTestingController;
  let pageService: PageService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent, PodcastComponent, FeedsComponent ],
      imports: [
        HttpClientTestingModule,  
        CommonModule,
        FormsModule,
        HomeRoutingModule,
        MaterialModule,
        SharedModule,
        RouterModule,
        BrowserAnimationsModule,
        NgxMasonryModule,
        LazyLoadImageModule,
        RouterTestingModule.withRoutes(
          [{path:'', component: HomeComponent}]
        )]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it ('ExistDisability must be false in Method: getData ', () => {
    pageService = TestBed.get(PageService);
    httpTestingController = TestBed.get(HttpTestingController);

    let dataMock = {
      "status": 200,
      "content": {
        "header": {
          "color": "#fffdea",
          "background": {
            "desktop": "https://storage.googleapis.com/mercati-products/angular/home/profile-image-banner.png",
            "mobile": "https://storage.googleapis.com/mercati-products/angular/home/micro-banner.jpg"
          },
          "title": "Este es un titulo de prueba",
          "summary": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
          "tag": "Trending Episode",
          "tagLower": "Listen On Also",
          "logos": [
            {
              "image": "https://storage.googleapis.com/mercati-products/angular/logos/apple-podcast.svg",
              "url": "/"
            },
            {
              "image": "https://storage.googleapis.com/mercati-products/angular/logos/apple-podcast.svg",
              "url": "/"
            },
            {
              "image": "https://storage.googleapis.com/mercati-products/angular/logos/spotify.svg",
              "url": "/"
            }
          ]
        },
        "blog": {
          "title": "Blog post",
          "cards": [
            {
              "name": "By Mehedi Hasan",
              "date": " Sep 11, 2020",
              "by": "Cantonese Radio",
              "title": "Global News Podcast Thai soldier kills many people a shooting rampage",
              "summary": "Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..",
              "button": {
                "url": "/",
                "title": "Read More"
              }
            },
            {
              "name": "By Mehedi Hasan",
              "date": " Sep 11, 2020",
              "by": "Cantonese Radio",
              "title": "Global News Podcast Thai soldier kills many people a shooting rampage",
              "summary": "Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..",
              "button": {
                "url": "/",
                "title": "Read More"
              }
            },
            {
              "name": "By Mehedi Hasan",
              "date": " Sep 11, 2020",
              "by": "Cantonese Radio",
              "title": "Global News Podcast Thai soldier kills many people a shooting rampage",
              "summary": "Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..",
              "button": {
                "url": "/",
                "title": "Read More"
              }
            },
            {
              "name": "By Mehedi Hasan",
              "date": " Sep 11, 2020",
              "by": "Cantonese Radio",
              "title": "Global News Podcast Thai soldier kills many people a shooting rampage",
              "summary": "Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..",
              "button": {
                "url": "/",
                "title": "Read More"
              }
            },
            {
              "name": "By Mehedi Hasan",
              "date": " Sep 11, 2020",
              "by": "Cantonese Radio",
              "title": "Global News Podcast Thai soldier kills many people a shooting rampage",
              "summary": "Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..",
              "button": {
                "url": "/",
                "title": "Read More"
              }
            }
          ]
        }
      }
    }
    
    //component.getData();
    const req = httpTestingController.expectOne('https://demo5346601.mockable.io/api/content/page=/');
    expect(req.request.method).toEqual('GET');
    req.flush(dataMock)

    let data: Home = new Home();
    data = component.data
    expect(component.status).toBe(200);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
