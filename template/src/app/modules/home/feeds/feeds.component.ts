import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.component.html',
  styleUrls: ['./feeds.component.scss']
})
export class FeedsComponent implements OnInit {
  feeds = [
    {
      image: '/assets/images/home/instagram/thumbnail-micro.jpg',
      alt: "imagen micrófono"
    },
    {
      image: '/assets/images/home/instagram/paleta.jpeg',
      alt: "imagen paleta"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-books.jpg',
      alt: "imagen libro"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-micro-2.jpg',
      alt: "imagen micrófono"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-banana.jpg',
      alt: "imagen banana"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-aircraft.jpg',
      alt: "imagen avión"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-water.jpg',
      alt: "imagen agua"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-soccer.jpg',
      alt: "imagen soccer"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-gift.jpg',
      alt: "imagen regalo"
    },
    {
      image: '/assets/images/home/instagram/thumbnail-vegetables.jpg',
      alt: "imagen vegetables"
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
