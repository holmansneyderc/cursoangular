import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { 
    this.formGroup = this.formBuilder.group({
      name: new FormControl('',[
        Validators.required,
        Validators.maxLength(20)
      ]),
      phone: new FormControl('',[
        Validators.required
      ]),
      address: new FormControl('',[
        Validators.email
      ]),
      message: new FormControl('',[
        Validators.required
      ])
    })
  }
  public message: string = ""
  formGroup: FormGroup;
  public result: number = 0

  ngOnInit(): void {

  }

  sumInt(a: number, b: number){
    let result = a + b;
    this.result = result;
  }

  validateResponse(data:string){
    this.message = data+"3"
  }


  sendContactForm(e: any): void {
    e.preventDefault();
    if (this.formGroup.invalid) {
      return;
    }
    console.log(this.formGroup.value)
  }
}
