import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractControl, FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared.module';

import { NewsletterComponent } from './newsletter.component';
//f para ejecutar una unica prueba
//x para omitir esta prueba
fdescribe('NewsletterComponent', () => { 
  let component: NewsletterComponent;
  let fixture: ComponentFixture<NewsletterComponent>;

  beforeEach(async () => {   
    //configureTestingModule => insumos para la prueba unitaria (Se deben importar todos los modulos o elementos que se requieran) 
    await TestBed.configureTestingModule({
      declarations: [ NewsletterComponent ],
      imports: [
        SharedModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsletterComponent);
    component = fixture.componentInstance;
    component = new NewsletterComponent(new FormBuilder());
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //cada it es una prueba a realizar
  it ('validate the fields exist', () => {
    expect( component.formGroup.contains('name')).toBeTruthy();
  })

  //Validate sum
  it ('validate the sum', () => {
    const resp = component.sumInt(8, 8)
    expect( component.result ).toBe(16);
  })

  //Validate sum
  it ('validate the sum', () => {
    const resp = component.sumInt(8, 8)
    expect( component.result ).toBe(16);
  })


  //Validate return info
  it ('validate the return info', () => {
    const resp = component.validateResponse("holman")
    expect( component.message ).toBe("holman3");
  })
  

  //f para ejecutar una unica prueba
  //x para omitir esta prueba
  it ('Phone field must be filled', () => {
    let control = component.formGroup.get('name');
    // control.setValue('');
    // expect( control.valid ).toBeFalsy();
  })


});
