import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  posts = [
    {
      name: "By Mehedi Hasan",
      date:'Sep 11, 2020',
      category:'Cantonese Radio',
      title:'Global News Podcast Thai soldier kills many people a shooting rampage',
      description:'Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..',
      link:'/'
    },
    {
      name: "By Mehedi Hasan",
      date:'Sep 11, 2020',
      category:'Cantonese Radio',
      title:'Global News Podcast Thai soldier kills many people a shooting rampage',
      description:'Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..',
      link:'/'
    },
    {
      name: "By Mehedi Hasan",
      date:'Sep 11, 2020',
      category:'Cantonese Radio',
      title:'Global News Podcast Thai soldier kills many people a shooting rampage',
      description:'<p>Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..</p>',
      link:'/'
    },
    {
      name: "By Mehedi Hasan",
      date:'Sep 11, 2020',
      category:'Cantonese Radio',
      title:'Global News Podcast Thai soldier kills many people a shooting rampage',
      description:'Lorem ipsum dolor sit amet, consetetur  sadipscing elitr, sed diam nonumy eirmod  tempor invidunt ut labore et dolore ………..',
      link:'/'
    }
  ];

  slideConfig = {
    arrows: true,
    slidesToShow: 3,
    variableWidth:true,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          variableWidth:true,
          slidesToShow: 2
        }
      },
    ]
  };
  constructor() { }

  ngOnInit(): void {
  }

}
