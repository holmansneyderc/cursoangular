import { Component, OnInit,HostListener ,Inject, ViewChild,ElementRef} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  toggleMenu:boolean = false;
  @ViewChild('menu', { static: false }) menu!: ElementRef;
  constructor() {
  }

  ngOnInit(): void {
  }
  
  openMenu():void{
    this.toggleMenu = !this.toggleMenu;
  }

  @HostListener('window:scroll', ['$event']) onScroll($event: Event): void {
    if (window.pageYOffset >= 50) {
      this.menu.nativeElement.classList.add('on-scroll');
    }
    else{
      this.menu.nativeElement.classList.remove('on-scroll');
    }
  }
}
