import { Component, Input, OnInit } from '@angular/core';
import { Banner } from '../../models/banner';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  @Input() data:Banner = new Banner();
  constructor() {
    console.log("1")
   }

  ngOnInit(): void {
    console.log("2")
  } 
  
  ngOnDestroy(){}
}
