import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PageService {
  private environment = environment;
  constructor(private http: HttpClient) { 
    
  }
  getPage(){
    return this.http.get(`${environment.api}/api/content/page=/`);
  }

  sendDataUser(){
    return this.http.post(`${environment.api}/api/content/form`, {});
  }
}
