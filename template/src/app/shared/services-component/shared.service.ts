import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private _listners = new Subject<any>();
  constructor() { }

  listen(): Observable<any> {
    return this._listners.asObservable();
  }

  action() {
    this._listners.next();
  }
}
