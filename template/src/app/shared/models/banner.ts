export class Banner{
    title: string = ""
    background: Picture = new Picture
    color: string = ""
    summary: string = ""
    tagLower: string = ""
    logos: Logo[] = []
    tag: string = ""
}

export class Picture{
    desktop: string = ""
    mobile: string = ""
}

export class Logo{
    url: string = ""
    image: string = ""
}
