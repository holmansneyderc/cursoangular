import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './layout/header/header.component';
import { BannerComponent } from './layout/banner/banner.component';
import { FooterComponent } from './layout/footer/footer.component';
import { NavComponent } from './layout/nav/nav.component';
import { BlogComponent } from './layout/blog/blog.component';
import { NewsletterComponent } from './layout/newsletter/newsletter.component';
import { RouterModule } from '@angular/router';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HeaderComponent,
    BannerComponent,
    FooterComponent,
    NavComponent,
    BlogComponent,
    NewsletterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SlickCarouselModule,
    MatFormFieldModule,
    MatInputModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ], 
  exports: [
    HeaderComponent,
    BannerComponent,
    FooterComponent,
    NavComponent,
    BlogComponent,
    RouterModule,
    NewsletterComponent,
    SlickCarouselModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: []
})
export class SharedModule { }
