import { environment } from 'src/environments/environment';
const { SitemapStream, streamToPromise } = require('sitemap')
const { createGzip } = require('zlib')


export class Sitemap {
    createSitemap(app: any): void {
        let sitemap;

        app.route('/sitemap.xml').get(function(req: any, res: any) {
            res.header('Content-Type', 'application/xml');
            res.header('Content-Encoding', 'gzip');
            // if we have a cached entry send it
         
            try {
              generateSitemap(res);
              
            } catch (e) {
              console.error(e)
              res.status(500).end()
            }
          })
        
          function generateSitemap(res: any){
            const smStream = new SitemapStream({ hostname: environment.host })
            const pipeline = smStream.pipe(createGzip())
        
            // pipe your entries or directly write them.
            smStream.write({ url: '/page-1/',  changefreq: 'daily', priority: 0.3 })
            smStream.write({ url: '/page-2/',  changefreq: 'monthly',  priority: 0.7 })
            smStream.write({ url: '/page-3/'})    // changefreq: 'weekly',  priority: 0.5
            smStream.write({ url: '/page-4/',   img: "http://urlTest.com" })
            /* or use
            Readable.from([{url: '/page-1'}...]).pipe(smStream)
            if you are looking to avoid writing your own loop.
            */
        
            // cache the response
            streamToPromise(pipeline).then((sm: any) => sitemap = sm)
            // make sure to attach a write stream such as streamToPromise before ending
            smStream.end()
            // stream write the response
            pipeline.pipe(res).on('error', (e: any) => {throw e})
          }
    }
}